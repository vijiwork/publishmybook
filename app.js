const express = require("express");
const cors = require("cors");
const path = require("path");

const app = express();
app.use(cors());
app.use(express.json());

app.use(express.static(path.join(__dirname, "./public")));

app.set("port", process.env.PORT || 8081);

app.listen(app.get("port"), () =>
  console.log(`Server listenling on port ${app.get("port")}`)
);
